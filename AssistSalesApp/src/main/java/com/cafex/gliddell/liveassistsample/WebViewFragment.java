package com.cafex.gliddell.liveassistsample;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

/**
 * WebViewFragment holds the WebView to display and the ProgressBar for its loading.
 */
public class WebViewFragment extends Fragment {
    /**
     * Our view component for the web page.
     */
    WebView webView;
    /**
     * Our view components for the progress bar.
     */
    ProgressBar progressBar;
    /** Keep track of what we think the website is so that if it changes we know to update **/
    private String currentWebsiteUrl = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_web, container, false);

        webView = (WebView) rootView.findViewById(R.id.webView);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);

        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {

                if ((progress < 100) && (progressBar.getVisibility() == ProgressBar.GONE)){
                    progressBar.setVisibility(ProgressBar.VISIBLE);
                }
                progressBar.setProgress(progress);
                if (progress == 100) {
                    progressBar.setVisibility(ProgressBar.GONE);
                }
            }

            @Override
            public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                Log.d("WebViewFragment", "onCreateWindow");

                //TODO need to handle this properly
                return true;
            }
        });

        //get webview to handle links itself
        WebViewClient webViewClient = new WebViewClient();
        webView.setWebViewClient(webViewClient);

        //enable JS
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setSupportMultipleWindows(true);
        WebView.setWebContentsDebuggingEnabled(true);

        //restore any state the webview needs to
        webView.restoreState(savedInstanceState);

        //restore our state of what the current preference URL was
        if (savedInstanceState != null) {
            String urlKey = getString(R.string.cached_url_key);
            currentWebsiteUrl = savedInstanceState.getString(urlKey);
        }

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        //do we have any URLs already loaded in our web view?
        String webViewCurrentUrl = webView.getUrl();

        //what is the current shared preferences web url?
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(getActivity());
        String preferenceUrl = preferences.getString(getString(R.string.website_url_key),
                getString(R.string.website_url_default));

        //if the preference URL has changed from when we first started, or if there is no page
        //loaded then we will load the url from the preferences page
        if ( webViewCurrentUrl == null || !preferenceUrl.equals(currentWebsiteUrl)) {
            webView.loadUrl(preferenceUrl);
            currentWebsiteUrl = preferenceUrl;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //save our web view state so that we can resume it
        webView.saveState(outState);

        //record what the current preference URL is in case it changes when we are resumed
        outState.putString(getString(R.string.cached_url_key), currentWebsiteUrl);
    }

    public void toggleAssist(String assistId)
    {

        String url = webView.getUrl();
        String newUrl = null;

        if (assistId == null)
        {
            if (url.contains("?cid"))
            {
                //disable the assist session
                int paramsIndex = url.indexOf("?cid");

                newUrl = url.substring(0, paramsIndex);
            }
        }
        else
        {
            //enable the assist session
            newUrl = url.concat("?cid="+assistId);
        }


        if (newUrl != null)
            webView.loadUrl(newUrl);
    }
}
