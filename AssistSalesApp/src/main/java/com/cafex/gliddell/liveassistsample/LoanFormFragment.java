package com.cafex.gliddell.liveassistsample;


import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.alicecallsbob.assist.sdk.core.Assist;
import com.github.gcacace.signaturepad.views.SignaturePad;

import java.io.InputStream;
import java.net.URL;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoanFormFragment extends Fragment implements CompoundButton.OnCheckedChangeListener{

    /** Current signed state key constant **/
    private final static String IS_SIGNED_KEY = "com.cafex.gliddell.liveassist.issigned";

    /** Current signature image key constant **/
    private final static String SIG_BITMAP_KEY = "com.cafex.gliddell.liveassist.sigbitmap";

    /**have we signed the form yet? **/
    private boolean isSigned = false;

    /** keep track of the watermark in case it changes **/
    private String currentWatermarkUrl = "";

    /** watermark cache key **/
    private final static String WATERMARK_CACHE_KEY = "com.cafex.gliddell.watermark";

    public LoanFormFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View inflatedView;
        Resources resources = getResources();

        switch (resources.getConfiguration().orientation)
        {
            case Configuration.ORIENTATION_LANDSCAPE:
                inflatedView = inflater.inflate(R.layout.fragment_loan_form_landscape, container, false);
                break;

            case Configuration.ORIENTATION_PORTRAIT:
                inflatedView = inflater.inflate(R.layout.fragment_loan_form_portrait, container, false);
                break;

            default:
                inflatedView = inflater.inflate(R.layout.fragment_loan_form_landscape, container, false);
                break;
        }

        //hide the relevant views from the agent
        View view = inflatedView.findViewById(R.id.lawsuit_group);
        view.setTag(Assist.PRIVATE_VIEW_TAG, true);
        view = inflatedView.findViewById(R.id.bankrupt_group);
        view.setTag(Assist.PRIVATE_VIEW_TAG, true);
        view = inflatedView.findViewById(R.id.affirmCheckBox);
        view.setTag(Assist.PRIVATE_VIEW_TAG, true);

        //Drawable drawable = resources.getDrawable(R.drawable.onbank_logo);
        //inflatedView.setBackground(drawable);
        CheckBox affirmCheckbox = (CheckBox) inflatedView.findViewById(R.id.affirmCheckBox);
        affirmCheckbox.setOnCheckedChangeListener(this);

        //define the behaviour for when the clear button is pressed on the signature pad
        final Button clearButton = (Button) inflatedView.findViewById(R.id.clearButton);
        clearButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ((SignaturePad)getView().findViewById(R.id.signature_pad)).clear();
            }
        });

        //define the behaviour when the cancel button is pressed on the signature pad
        final Button cancelButton = (Button) inflatedView.findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SignaturePad sigPad = ((SignaturePad) getView().findViewById(R.id.signature_pad));
                sigPad.clear();
                sigPad.setTag(Assist.PRIVATE_VIEW_TAG, null);

                ((CheckBox) getView().findViewById(R.id.affirmCheckBox)).setChecked(false);
                getView().findViewById(R.id.signatureLayout).setVisibility(View.GONE);
                getView().findViewById(R.id.scrollView).setVisibility(View.VISIBLE);
            }
        });

        //define the behaviour for when the done button is pressed on the signature pad
        final Button doneButton = (Button) inflatedView.findViewById(R.id.doneButton);
        doneButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                /* Get bitmap from sig pad and clear it */
                SignaturePad sigPad = ((SignaturePad) getView().findViewById(R.id.signature_pad));
                Bitmap sigBitmap = sigPad.getTransparentSignatureBitmap();
                sigPad.clear();
                sigPad.setTag(Assist.PRIVATE_VIEW_TAG, null);

                /* set bitmap of signature on image view */
                ImageView sigImage = (ImageView) getView().findViewById(R.id.sigImage);
                //sigImage.setImageBitmap(sigBitmap);
                sigImage.setImageDrawable(new BitmapDrawable(null, sigBitmap));

                /*return to the form */
                getView().findViewById(R.id.signatureLayout).setVisibility(View.GONE);
                getView().findViewById(R.id.scrollView).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.signedLayout).setVisibility(View.VISIBLE);
            }
        });

        //get whether or not we are currently in a signed state
        if (savedInstanceState != null) {
            isSigned = savedInstanceState.getBoolean(IS_SIGNED_KEY);

            if (isSigned)
            {
                inflatedView.findViewById(R.id.signedLayout).setVisibility(View.VISIBLE);
                ImageView sigImage = (ImageView) inflatedView.findViewById(R.id.sigImage);
                sigImage.setImageDrawable(new BitmapDrawable(null,
                        (Bitmap) savedInstanceState.getParcelable(SIG_BITMAP_KEY)));
            }

            currentWatermarkUrl = savedInstanceState.getString(WATERMARK_CACHE_KEY);
        }

        //inflate the layout for this fragment
        return inflatedView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(IS_SIGNED_KEY, isSigned);
        outState.putString(WATERMARK_CACHE_KEY, currentWatermarkUrl);

        if (isSigned) {
            ImageView sigImage = (ImageView) getView().findViewById(R.id.sigImage);
            BitmapDrawable drawable = (BitmapDrawable) sigImage.getDrawable();

            if (drawable != null)
                outState.putParcelable(SIG_BITMAP_KEY, drawable.getBitmap());
        }
    }

    /**
     * Called when the checked state of a compound button has changed.
     *
     * @param checkBox The compound button view whose state has changed.
     * @param isChecked  The new checked state of buttonView.
     */
    @Override
    public void onCheckedChanged(CompoundButton checkBox, boolean isChecked) {
        View mainView = getView();

        if (isSigned != isChecked) {

            isSigned = isChecked;

            if (isChecked) {
                mainView.findViewById(R.id.signature_pad).setTag(Assist.PRIVATE_VIEW_TAG, true);
                mainView.findViewById(R.id.signatureLayout).setVisibility(View.VISIBLE);
                mainView.findViewById(R.id.scrollView).setVisibility(View.INVISIBLE);
                mainView.findViewById(R.id.signedLayout).setVisibility(View.INVISIBLE);
            } else {
            /* remove sigpad from view */
                mainView.findViewById(R.id.signedLayout).setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        //what is the current shared preferences web url?
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(getActivity());
        final String watermarkUrl = preferences.getString(getString(R.string.watermark_url_key),
                getString(R.string.watermark_url_default));

        //if the watermark URL is set and is different to the original watermark URL
        if ( !watermarkUrl.equals("") && !watermarkUrl.equals(currentWatermarkUrl)) {

            //network thread for getting image
            new Thread(new Runnable()
            {
                public void run()
                {
                    try{
                        final Bitmap watermarkBitmap = BitmapFactory.decodeStream((InputStream) new URL(watermarkUrl).getContent());

                        //ui thread for updating watermark
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //load watermark from URL and set as image
                                ImageView watermarkImage = (ImageView) getView().findViewById(R.id.watermark);
                                watermarkImage.setImageBitmap(watermarkBitmap);
                            }
                        });
                    } catch (Exception e) {
                        Toast toast = Toast.makeText(getView().getContext(), "Failed to get watermark: "+e, Toast.LENGTH_SHORT);
                        toast.show();
                    }
                }
            }).start();
        }
    }
}
