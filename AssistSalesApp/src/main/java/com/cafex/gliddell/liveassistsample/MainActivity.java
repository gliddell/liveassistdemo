package com.cafex.gliddell.liveassistsample;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.internal.view.menu.ActionMenuItemView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.alicecallsbob.fcsdk.android.phone.VideoSurface;

import java.util.List;

public class MainActivity extends ActionBarActivity implements AssistSession.AssistSessionListener {

    /** key for our instance state on whether we are showing the web or the loan application form */
    public static final String IS_SHOWING_WEB = "com.cafex.liveassist.isShowingWeb";

    /** Simple state engine to know if la is in progress or not **/
    private boolean laStarted = false;

    /** Keeping track of how the video view is positioned. Reset means it is central of screen **/
    private boolean videoViewIsCentred = true;

    /** our Manager of the assist session **/
    private static AssistSession assistSession;

    /** Large video resolution point **/
    private static final Point LARGE_RESOLUTION = new Point(1600, 1200);

    /** Large video resolution point **/
    private static final Point SMALL_RESOLUTION = new Point(352, 240);

    /** Our web view fragment **/
    private WebViewFragment webViewFragment;

    /** Out Loan fragment **/
    private LoanFormFragment loanFragment;

    /** Track whether showing web or loan **/
    private boolean displayWeb = true;

    /*
     * Activity lifecycle methods
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //enable progress bar
        /* CANNOT GET THE WINDOW PROGRESS BAR TO WORK SO USING OWN PROGRESS BAR
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        requestWindowFeature(Window.FEATURE_PROGRESS); */

        super.onCreate(savedInstanceState);

        //create view
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (savedInstanceState == null) {
            webViewFragment = new WebViewFragment();
            loanFragment = new LoanFormFragment();
            fragmentManager.beginTransaction()
                    .add(R.id.contentContainer, loanFragment)
                    .hide(loanFragment)
                    .add(R.id.contentContainer, webViewFragment)
                    .commit();
        }
        else
        {
            List fragmentList = fragmentManager.getFragments();
            loanFragment = (LoanFormFragment)fragmentList.get(0);
            webViewFragment = (WebViewFragment)fragmentList.get(1);

            displayWeb = savedInstanceState.getBoolean(IS_SHOWING_WEB);

            if (displayWeb)
            {
                fragmentManager.beginTransaction()
                        .hide(loanFragment)
                        .commit();
            }
            else
            {
                fragmentManager.beginTransaction()
                        .hide(webViewFragment)
                        .commit();
            }
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //save our video view state
        View remoteVideoContainer = findViewById(R.id.remoteVideoContainer);
        outState.putBoolean(getString(R.string.videoViewIsVisible), remoteVideoContainer.isShown());

        //store whether we are currently running an LA session or not
        outState.putBoolean(getString(R.string.isLaStarted), laStarted);

        //store our view state
        outState.putBoolean(getString(R.string.videoViewIsCentred), videoViewIsCentred);

        //store what fragment we are showing
        outState.putBoolean(IS_SHOWING_WEB, displayWeb);

        //store our assist session
        if (assistSession != null) {
            assistSession.setListener(null);

            //commented out as we are hacking a static variable for this
            //outState.putSerializable(getString(R.string.assistSession), assistSession);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        //restore our view state
        videoViewIsCentred = savedInstanceState.getBoolean(getString(R.string.videoViewIsCentred));

        //restore whether we are currently running an LA session or not
        laStarted = savedInstanceState.getBoolean(getString(R.string.isLaStarted));

        //retrieve our assist session if it exists
        //commented out as we are hacking a static variable for this
        //assistSession = (AssistSession) savedInstanceState.getSerializable(getString(R.string.assistSession));

        if (assistSession != null) {
            assistSession.setListener(this);
            ViewGroup videoContainer = (ViewGroup) findViewById(R.id.remoteVideoContainer);
            Log.d("MainActivity", "Viewgroup Size = "+videoContainer.getChildCount());
            VideoSurface videoSurface = assistSession.getNewSurface();
            videoContainer.addView(videoSurface, 1);
            findViewById(R.id.videoProgressBar).setVisibility(View.GONE);
        }

        //restore the position of our video state if it was viewable before
        if (savedInstanceState.getBoolean(getString(R.string.videoViewIsVisible))) {
            View videoView = findViewById(R.id.remoteVideoContainer);
            videoView.setVisibility(View.VISIBLE);
            updateVideoView(videoViewIsCentred);
            updateContentView(videoViewIsCentred);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch(id) {

            case R.id.action_settings:
                openSettings();
                return true;

            case R.id.action_la:
                toggleLa();
                return true;

            case R.id.action_loan:
                getSupportFragmentManager().beginTransaction()
                        .show(loanFragment).hide(webViewFragment).commit();
                displayWeb = false;
                return true;

            case R.id.action_web:
                getSupportFragmentManager().beginTransaction()
                        .show(webViewFragment).hide(loanFragment).commit();
                displayWeb = true;
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Open the settings screen
     */
    private void openSettings()
    {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    /**
     * Toggle LA on and off
     */
    private void toggleLa()
    {
        //now start the process
        if (laStarted)
        {
            assistSession.stopAssist();
        } else
        {
            startAssist();
        }

        updateAssistIcon();
    }

    private void updateAssistIcon() {
        //update live assist button so it can be seen as a toggle switch
        ActionMenuItemView laButton = (ActionMenuItemView) findViewById(R.id.action_la);
        Resources res = getResources();
        laButton.setIcon(laStarted ?
                res.getDrawable(R.drawable.live_assist_circle_orange) :
                res.getDrawable(R.drawable.live_assist_circle_white));
    }

    private void startAssist()
    {
        //we have started the la process
        laStarted = true;

        //show video view with progress bar
        View remoteVideoContainer = findViewById(R.id.remoteVideoContainer);
        remoteVideoContainer.setVisibility(View.VISIBLE);
        View progressbar = findViewById(R.id.videoProgressBar);
        progressbar.setVisibility(View.VISIBLE);

        //to start a video call, we first need to get a session token from the server
        //what is the current shared preferences la url?
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(this);
        final String laUrlString = preferences.getString(getString(R.string.la_server_url_key),
                getString(R.string.la_server_url_default));

        //we also need to know if we are running a native assist session, or whether we are letting
        //the web page run its own.
        final boolean nativeSession = preferences.getBoolean(getString(R.string.la_native_key), true);

        //now invoke the consumer service to get a session on a new thread
        assistSession = AssistSession.getNewAssistSession(laUrlString, this, getApplication(), LARGE_RESOLUTION, nativeSession);
    }

    public void assistStopped()
    {
        //set our flag to stopped
        laStarted = false;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //hide video view
                ViewGroup remoteVideoContainer = (ViewGroup) findViewById(R.id.remoteVideoContainer);
                remoteVideoContainer.setVisibility(View.INVISIBLE);

                if (remoteVideoContainer.getChildCount() > 1) {
                    remoteVideoContainer.removeViewAt(1);
                }

                //remove our handle to the assist session
                assistSession = null;

                updateVideoView(true);
                updateContentView(true);
                webViewFragment.toggleAssist(null);

            }
        });
    }

    public void remoteVideoClicked(View view)
    {
        //toggle our video view being centred
        centreVideo(!videoViewIsCentred);
    }

    private void centreVideo(boolean centre) {
        videoViewIsCentred = centre;
        updateVideoView(videoViewIsCentred);
        updateContentView(videoViewIsCentred);
    }

    /**
     * update the content view screen layout. true to centre it, false to move it to the side of the
     * video view
     */
    private void updateContentView(boolean centre) {
        View contentView = findViewById(R.id.contentContainer);

        RelativeLayout.LayoutParams origLayoutParams = (RelativeLayout.LayoutParams)contentView.getLayoutParams();
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(origLayoutParams);

        if (centre) {
            layoutParams.removeRule(RelativeLayout.RIGHT_OF);
        }
        else {
            layoutParams.addRule(RelativeLayout.RIGHT_OF, R.id.remoteVideoContainer);
        }
        contentView.setLayoutParams(layoutParams);
    }

    /**
     * update the video screen layout when it is moved by clicking on the image
     */
    private void updateVideoView(boolean reset) {

        ViewGroup videoContainer = (ViewGroup)findViewById(R.id.remoteVideoContainer);

        RelativeLayout.LayoutParams origLayoutParams = (RelativeLayout.LayoutParams)videoContainer.getLayoutParams();
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(origLayoutParams);

        Point resolution;

        if (reset)
        {
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
            layoutParams.removeRule(RelativeLayout.ALIGN_PARENT_LEFT);
            resolution = LARGE_RESOLUTION;
        }
        else {
            layoutParams.removeRule(RelativeLayout.CENTER_IN_PARENT);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            resolution = SMALL_RESOLUTION;
        }

        /*
        Set our video dimensions
         */
        VideoSurface videoSurface = (VideoSurface) videoContainer.getChildAt(1);

        if (videoSurface != null){
            videoSurface.setDimensions(resolution);
        }

        videoContainer.setLayoutParams(layoutParams);
    }

    @Override
    public void assistCreationFailure(String failureString) {
    }

    @Override
    public void setVideoSurface(final VideoSurface surface) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ViewGroup videoContainer = (ViewGroup) findViewById(R.id.remoteVideoContainer);

                if (videoContainer.getChildCount() == 1) {
                    videoContainer.addView(surface, 1);
                    findViewById(R.id.videoProgressBar).setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void cobrowseStarted() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (videoViewIsCentred)
                {
                    centreVideo(false);
                }
            }
        });
    }

    @Override
    public void startWebViewCobrowse(final String cid) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                webViewFragment.toggleAssist(cid);
            }
        });
    }
}
