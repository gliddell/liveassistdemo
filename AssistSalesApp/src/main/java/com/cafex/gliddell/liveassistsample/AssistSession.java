package com.cafex.gliddell.liveassistsample;

import android.app.Application;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;

import com.alicecallsbob.assist.sdk.config.AssistConfig;
import com.alicecallsbob.assist.sdk.config.impl.AssistConfigBuilder;
import com.alicecallsbob.assist.sdk.core.Assist;
import com.alicecallsbob.assist.sdk.core.AssistError;
import com.alicecallsbob.assist.sdk.core.AssistListener;
import com.alicecallsbob.fcsdk.android.phone.Call;
import com.alicecallsbob.fcsdk.android.phone.CallCreationException;
import com.alicecallsbob.fcsdk.android.phone.CallListener;
import com.alicecallsbob.fcsdk.android.phone.CallStatus;
import com.alicecallsbob.fcsdk.android.phone.Phone;
import com.alicecallsbob.fcsdk.android.phone.VideoSurface;
import com.alicecallsbob.fcsdk.android.phone.VideoSurfaceListener;
import com.alicecallsbob.fcsdk.android.uc.UC;
import com.alicecallsbob.fcsdk.android.uc.UCFactory;
import com.alicecallsbob.fcsdk.android.uc.UCListener;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.TrustManager;

/**
 * Class for handling all network aspects of the Assist Session. This will create the session,
 * maintain the network connection, create the video call etc..
 *
 * All UI will be handled by the controlling activity.
 */
public class AssistSession implements UCListener, CallListener, VideoSurfaceListener, AssistListener, Serializable {
    /**
     * Define our logging context name *
     */
    public static final String LOG_TAG = AssistSession.class.getSimpleName();
    private String laUrlString;
    private UC uc;
    private Phone phone;
    private Call call;
    private transient VideoSurface surface;
    private AssistSessionListener listener;
    private final static int HTTP_RESPONSE_OK = 200;
    private Application application;
    private Point videoDimensions;
    private boolean nativeLa;

    /**
     * Factory method to get an instance of this class and to have it start its network
     * io on a thread
     * @param laUrlString The URL of the LA server to connect to
     */
    public static AssistSession getNewAssistSession(String laUrlString,
                                                    AssistSessionListener listener,
                                                    Application application,
                                                    Point videoDimensions,
                                                    boolean nativeLa)
    {
        final AssistSession sessionManager = new AssistSession(laUrlString, listener, application, videoDimensions, nativeLa);
        new Thread(new Runnable() {
            @Override
            public void run() {
                sessionManager.createNewSession();
            }
        }).start();
        return sessionManager;
    }

    /**
     * Constructor only called from factory method.
     * @param laUrlString The URL of the LA server to connect to
     * @param listener A listener for the session to call back to on different events
     */
    private AssistSession(String laUrlString, AssistSessionListener listener,
                          Application application, Point videoDiemensions, boolean nativeLa) {
        this.laUrlString = laUrlString;
        this.listener = listener;
        this.application = application;
        this.videoDimensions = videoDiemensions;
        this.nativeLa = nativeLa;
    }

    /**
     * Creates new session state on the LA server to kick off the LA session.
     */
    private void createNewSession() {
        try {

            /*
            Use the HTTP session service inherent within the LA solution to create the session
            for the video calling
             */
            final URL serverUrl = new URL(laUrlString);
            final String base64Url = Base64.encodeToString(serverUrl.getHost().getBytes(), Base64.DEFAULT);
            final String urlEncodedQueryParams = URLEncoder.encode(base64Url, "UTF-8");

            final String getSessionUrl = laUrlString + application.getString(R.string.consumerUrlSuffix) + urlEncodedQueryParams;

            Log.d(LOG_TAG, getSessionUrl);

            final HttpPost get = new HttpPost(getSessionUrl);
            final DefaultHttpClient client = new DefaultHttpClient();

            HttpResponse response = client.execute(get);
            HttpEntity responseEntity = response.getEntity();
            String responseString = EntityUtils.toString(responseEntity);

            /*
            check that we created the FCSDK session successfully.
             */
            if (response.getStatusLine().getStatusCode() == HTTP_RESPONSE_OK) {

                Log.d(LOG_TAG, responseString);
                JSONObject jsonObject = new JSONObject(responseString);
                String sessionToken = jsonObject.getString(application.getString(R.string.assist_session_token));
                String userId = jsonObject.getString(application.getString(R.string.assist_address));

                Log.d(LOG_TAG, "Session Token: " + sessionToken);
                Log.d(LOG_TAG, "UserID: " + userId);

                if (nativeLa)
                    startCobrowse(userId);
                else
                    listener.startWebViewCobrowse(userId);

                startVideo(sessionToken, userId);
            } else {
                Log.e(LOG_TAG, "Failed to get session ID: " + responseString);
                listener.assistCreationFailure(application.getString(R.string.session_creation_error));
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Failed to create session", e);
            listener.assistCreationFailure(application.getString(R.string.session_creation_error));
        } catch (JSONException jsone) {
            Log.e(LOG_TAG, "Failed to parse JSON", jsone);
            listener.assistCreationFailure(application.getString(R.string.session_creation_error));
        } catch (Exception e) {
            Log.e(LOG_TAG, "Uncaught Exception", e);
            listener.assistCreationFailure(application.getString(R.string.session_creation_error));
        }
    }

    /**
     * Create the assist session and connect to it.
     * @param cobrowseId
     * @throws MalformedURLException
     */
    private void startCobrowse(String cobrowseId) throws MalformedURLException {
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(application);

        String la_server_url = preferences.getString(application.getString(R.string.la_server_url_key),
                application.getString(R.string.la_server_url_default));

        try {

            URL url = new URL(preferences.getString(application.getString(R.string.la_server_url_key), la_server_url));
            boolean secure = url.getProtocol().equals("https");
            int defaultPort = secure ? 443 : 80;
            AssistConfig config = new AssistConfigBuilder(application).
                    setServerHost(url.getHost()).
                    setServerPort(url.getPort() == -1 ? defaultPort : url.getPort()).
                    setCorrelationId(cobrowseId).
                    setAgentName("").
                    setConnectSecurely(secure).
                    setTrustManager(new TrustManager() {
                    }).
                    build();

            Assist.startSupport(config, application, this);

        } catch (MalformedURLException mue) {
            Log.e(LOG_TAG, "LA Server URL is not valid: " + la_server_url);
            throw mue;
        }
    }

    /**
     * Start the video call.
     * @param sessionToken
     * @param userId
     */
    private void startVideo(String sessionToken, String userId) {
        uc = UCFactory.createUc(application, sessionToken, this);

        //TODO need to properly implement network change listener
        uc.setNetworkReachable(true);
        uc.startSession();
    }

    /**
     * Stops the assist session killing both the video call and the assist session itself.
     */
    public void stopAssist()
    {
        //all tear down is triggered off the call finishing
        call.end();

        //nasty hack as we are not getting a call back when we end the call
        onStatusChanged(call, CallStatus.ENDED);
    }

    /**
     * UCListener methods *
     */
    @Override
    public void onSessionStarted() {

        phone = uc.getPhone();

        listener.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                startVideoCall();
            }
        });
    }

    private void startVideoCall() {
        //start the call
        try {
            SharedPreferences preferences = PreferenceManager
                    .getDefaultSharedPreferences(application);
            String la_agent_name = preferences.getString(application.getString(R.string.la_agent_key),
                    application.getString(R.string.la_agent_default));
            surface = phone.createVideoSurface(application, videoDimensions, this);
            call = phone.createCall(la_agent_name, true, true, this);
            call.setVideoView(surface);
        } catch (CallCreationException cce) {
            Log.e(LOG_TAG, "Call Creation Failed", cce);
        } catch (Exception e) {
            Log.e(LOG_TAG, "Uncaught Exception", e);
        }
    }

    VideoSurface getNewSurface()
    {
        surface = phone.createVideoSurface(application, videoDimensions, this);
        call.setVideoView(surface);
        return surface;
    }

    void setListener(AssistSessionListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onSessionNotStarted() {

    }

    @Override
    public void onSystemFailure() {

    }

    @Override
    public void onConnectionLost() {

    }

    @Override
    public void onConnectionRetry(int i, long l) {

    }

    @Override
    public void onConnectionReestablished() {

    }

    @Override
    public void onGenericError(String s, String s2) {

    }

    /**
     * VideoSurfaceListener Methods *
     */
    @Override
    public void onFrameSizeChanged(int i, int i2, VideoSurface.Endpoint endpoint, VideoSurface videoSurface) {
        Log.d(LOG_TAG, "onFrameSizeChanged");
    }

    @Override
    public void onSurfaceRenderingStarted(final VideoSurface videoSurface) {

        Log.d(LOG_TAG, "onSurfaceRenderingStarted");
    }

    /**
     * CallListener overrides *
     */

    @Override
    public void onDialFailed(Call call, String s, CallStatus callStatus) {

    }

    @Override
    public void onCallFailed(Call call, String s, CallStatus callStatus) {

    }

    @Override
    public void onMediaChangeRequested(Call call, boolean b, boolean b2) {

    }

    @Override
    public void onStatusChanged(Call call, CallStatus callStatus) {
        if (callStatus == CallStatus.ENDED)
        {
            if (nativeLa)
                Assist.endSupport();
            listener.assistStopped();
            listener = null;

            //causes a crash when ending the call from our side so removing for the moment
            //uc.stopSession();
        }
    }

    @Override
    public void onRemoteDisplayNameChanged(Call call, String s) {

    }

    @Override
    public void onRemoteMediaStream(Call call) {
        if (listener!=null) listener.setVideoSurface(surface);
    }

    @Override
    public void onInboundQualityChanged(Call call, int i) {

    }

    @Override
    public void onRemoteHeld(Call call) {

    }

    @Override
    public void onRemoteUnheld(Call call) {

    }

    /*
     * AssistListener methods
     */
    @Override
    public void onSupportEnded(boolean b) {
        Log.d(LOG_TAG, "onSupportEnded: "+b);
    }

    @Override
    public void onSupportError(AssistError assistError, String s) {

    }

    /**
     * Created by gethinliddell on 09/04/2015.
     */
    public static interface AssistSessionListener {

        public void assistCreationFailure(String failureString);
        public void runOnUiThread(Runnable runnable);
        public void setVideoSurface(VideoSurface surface);
        public void assistStopped();
        public void cobrowseStarted();
        public void startWebViewCobrowse(String cid);

    }
}